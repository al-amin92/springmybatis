<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script>
    $('#btnShow').click(function(){
        $('#foo').show();
    });

    $('#btnHide').click(function(){
        $('#foo').hide();
    });

    (function ($) {
        $.each(['show', 'hide'], function (i, ev) {
            var el = $.fn[ev];
            $.fn[ev] = function () {
                this.trigger(ev);
                return el.apply(this, arguments);
            };
        });
    })(jQuery);


    $('#foo').on('show', function(){
        $('#console').html( $('#console').html() + '#foo is now visible'+ '<br>'  )
    });

    $('#foo').on('hide', function(){
        $('#console').html( $('#console').html() + '#foo is hidden'+ '<br>'  )
    });
</script>
<html>
<head>
    <title><c:out value="Person"/></title>
</head>
<body align="center">

<div>
    <button id="btnShow">Show</button>
    <button id="btnHide">Hide</button>
    <div class="container">
        <div id="foo">
            I am #foo
        </div>
    </div>
    <div id="console">
    </div>
</div>


<h3><c:out value="Individual Person"/></h3>
<springForm:form action="update" commandName="person" method="POST">
    <table align="center">
        <tr>
            <td>
                <b><c:out value="person name"/></b>
            </td>
            <td>
                <b><c:out value="company"/></b>
            </td>
            <td>
                <b><c:out value="mobileNo"/></b>
            </td>
            <td>
                <b><c:out value="joiningDate"/></b>
            </td>
            <td>
                <b><c:out value="age"/></b>
            </td>
            <td>
                <b><c:out value="update"/></b>
            </td>
        </tr>
        <tr>
            <td>
                <springForm:input path="name"/>
                <springForm:errors path="name" cssStyle="color: red;"/>
            </td>
            <td>
                <springForm:select path="companyId">
                    <c:forEach items="${companyList}" var="myCompany">
                        <springForm:option value="${myCompany.id}"><c:out
                                value="${myCompany.name}"/></springForm:option>
                    </c:forEach>
                </springForm:select>
            </td>
            <td>
                <springForm:input path="mobileNo"/>
            </td>
            <td>
                <springForm:input type="date" path="joiningDate"/>
            </td>
            <td>
                <springForm:input path="age"/>
            </td>
            <td>
                <input type="submit"/>
            </td>
        </tr>
    </table>
</springForm:form>
</body>
</html>
