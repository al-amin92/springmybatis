<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Company"/></title>
</head>
<body align="center">
<h3><c:out value="Individual Company"/></h3>
<springForm:form action="update" commandName="company" method="POST">
    <table align="center">
        <tr>
            <td>
                <b><c:out value="company name"/></b>
            </td>
            <td>
                <b><c:out value="company details"/></b>
            </td>
            <td>
                <b><c:out value="country"/></b>
            </td>
            <td>
                <b><c:out value="update"/></b>
            </td>
        </tr>
        <tr>
            <td>
                <springForm:input path="name"/>
                <springForm:errors path="name" cssStyle="color: red;"/>
            </td>
            <td>
                <springForm:input path="details"/>
                <springForm:errors path="details" cssStyle="color: red;"/>
            </td>
            <td>
                <springForm:input path="country"/>
            </td>
            <td>
                <input type="submit"/>
            </td>
        </tr>
    </table>
</springForm:form>
</body>
</html>
