<%@ taglib prefix="springForm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><c:out value="Spring MyBatis"/></title>

<style>
#modify, #modify2, #showLess{
    display: none;
}
#showMore{
    display: block;
}
</style>

<script>
    function turnOn(){
        document.getElementById("modify").style.display="block";
        document.getElementById("modify2").style.display="block";
        document.getElementById("showMore").style.display="none";
        document.getElementById("showLess").style.display="block";
    }
    function turnOf(){
        document.getElementById("modify").style.display="none";
        document.getElementById("modify2").style.display="none";
        document.getElementById("showMore").style.display="block";
        document.getElementById("showLess").style.display="none";
    }
</script>

</head>

<body>

<div align="center">
    <br>

    <div style="color: green">
        <c:if test="${successLog ne null and successLog ne 'no'}">
            <c:out value="${successLog}"/>
            <c:set var="successLog" value="no" scope="session"/>
        </c:if>
    </div>
    <div style="color: red">
        <c:if test="${errorLog ne null and errorLog ne 'no'}">
            <c:out value="${errorLog}"/>
            <c:set var="errorLog" value="no" scope="session"/>
        </c:if>
    </div>
    <h3><c:out value="Person List"/></h3>
    <table>
        <tr>
            <td>
                <b><c:out value="person name"/></b>
            </td>
            <td>
                <b><c:out value="company id"/></b>
            </td>
            <td>
                <b><c:out value="company name"/></b>
            </td>
            <td>
                <b><c:out value="company details"/></b>
            </td>
            <td>
                <b><c:out value="joining date"/></b>
            </td>
            <td>
                <b><c:out value="mobile no."/></b>
            </td>
            <td>
                <b><c:out value="age"/></b>
            </td>
            <div id="modify">
            <td>
                <b><c:out value="update"/></b>
            </td>
            <td>
                <b><c:out value="delete"/></b>
            </td>
            </div>
        </tr>
        <c:forEach items="${personList}" var="person">

            <tr>
                <td>
                    <c:out value="${person.name}"/>
                </td>
                <td>
                    <c:out value="${person.company.id}"/>
                </td>
                <td>
                    <c:out value="${person.company.name}"/>
                </td>
                <td>
                    <c:out value="${person.company.details}"/>
                </td>
                <td>
                    <c:out value="${person.joiningDate}"/>
                </td>
                <td>
                    <c:out value="${person.mobileNo}"/>
                </td>
                <td>
                    <c:out value="${person.age}"/>
                </td>
                <div id="modify2">
                <td>
                    <form action="person/${person.id}/update" method="get">
                        <input type="submit" value="Update"/>
                    </form>
                </td>
                <td>
                    <form action="person/${person.id}/delete" method="POST">
                        <input type="submit" value="Delete"/>
                    </form>
                </td>
                </div>
                <td id="showMore">
                    <a href="#" onclick="turnOn()">Show More</a>
                </td>
                <td id="showLess">
                    <a href="#" onclick="turnOff()">Show Less</a>
                </td>
            </tr>
        </c:forEach>
    </table>

    <h3><c:out value="Company List"/></h3>
    <table>
        <tr>
            <td>
                <b><c:out value="company name"/></b>
            </td>
            <td>
                <b><c:out value="company details"/></b>
            </td>
            <td>
                <b><c:out value="country"/></b>
            </td>
            <td>
                <b><c:out value="update"/></b>
            </td>
            <td>
                <b><c:out value="delete"/></b>
            </td>
        </tr>
        <c:forEach items="${companyList}" var="company">

            <tr>
                <td>
                    <c:out value="${company.name}"/>
                </td>
                <td>
                    <c:out value="${company.details}"/>
                </td>
                <td>
                    <c:out value="${company.country}"/>
                </td>
                <td>
                    <form action="company/${company.id}/update" method="GET">
                        <input type="submit" value="Update"/>
                    </form>
                </td>
                <td>
                    <form action="company/${company.id}/delete" method="POST">
                        <input type="submit" value="Delete"/>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>


</div>
<div align="center">
    <h3><c:out value="Insert a Person"/></h3>
    <table>
        <springForm:form action="person/insert" commandName="person" method="POST">
            <tr>
                <td><c:out value="Name: "/><springForm:input path="name"/>
                    <springForm:errors path="name" cssStyle="color: red;"/>
                </td>
                <td><c:out value="Company: "/>
                    <select name="company_id">
                        <c:forEach items="${companyList}" var="myCompany">
                            <option value="${myCompany.id}"><c:out value="${myCompany.name}"/></option>
                        </c:forEach>
                    </select>
                </td>

                <td><c:out value="Date: "/><springForm:input type="date" path="joiningDate"/></td>
                <td><c:out value="Mobile No.: "/><springForm:input path="mobileNo"/></td>
                <td><c:out value="Age: "/><springForm:input path="age"/></td>

                <td><input type="submit"></td>
            </tr>
        </springForm:form>
    </table>
    <br>

    <h3><c:out value="Insert a Company"/></h3>
    <springForm:form action="company/insert" commandName="company" method="POST">
        <c:out value="Name: "/><springForm:input path="name"/>
        <springForm:errors path="name" cssStyle="color: red;"/>
        <c:out value="Details: "/><springForm:input path="details"/>
        <springForm:errors path="details" cssStyle="color: red;"/>
        <c:out value="Country: "/><springForm:input path="country"/>
        <input type="submit">
    </springForm:form>
</div>
<br><br><br><br>

</body>
</html>
