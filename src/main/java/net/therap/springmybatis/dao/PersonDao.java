package net.therap.springmybatis.dao;

import net.therap.springmybatis.entity.Person;
import net.therap.springmybatis.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author al-amin
 * @since 1/4/17
 */
@Repository
public class PersonDao {

    @Autowired
    private Mapper mapper;

    public List<Person> getAllPerson() {
        return mapper.selectAllPerson();
    }

    public Person getPersonById(int id) {
        return mapper.selectPerson(id);
    }

    public void insertPerson(Person person) {
        mapper.insertPerson(person);
    }

    public void updatePerson(Person person) {
        mapper.updatePerson(person);
    }

    public void deletePerson(Person person) {
        mapper.deletePerson(person);
    }
}
