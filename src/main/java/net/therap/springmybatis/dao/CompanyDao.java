package net.therap.springmybatis.dao;

import net.therap.springmybatis.entity.Company;
import net.therap.springmybatis.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author al-amin
 * @since 1/5/17
 */
@Repository
public class CompanyDao {

    @Autowired
    private Mapper mapper;

    public List<Company> getAllCompany() {
        return mapper.selectAllCompany();
    }

    public Company getCompany(int id) {
        return mapper.selectCompanyById(id);
    }

    public void insert(Company company) {
        mapper.insertCompany(company);
    }

    public void delete(Company company) {
        mapper.deleteCompany(company);
    }

    public void update(Company company) {
        mapper.updateCompany(company);
    }
}
