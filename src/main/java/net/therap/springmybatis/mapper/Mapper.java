package net.therap.springmybatis.mapper;

import net.therap.springmybatis.entity.Company;
import net.therap.springmybatis.entity.Details;
import net.therap.springmybatis.entity.Person;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author al-amin
 * @since 1/3/17
 */


public interface Mapper {

    public List<Person> selectAllPerson();

    //@Select("SELECT * FROM person WHERE id = #{id}")
    public Person selectPerson(@Param("id") int id);

    //@Select("SELECT * FROM company WHERE id = #{id}")
    public Company selectCompanyById(@Param("id") int id);

    //@Insert("INSERT INTO person (name) VALUES (#{name})")
    public int insertPerson(Person person);

    public Details selectDetails(int id);

    public void updatePerson(Person person);

    void deletePerson(Person person);

    public List<Company> selectAllCompany();

    void insertCompany(Company company);

    void deleteCompany(Company company);

    void updateCompany(Company company);
}