package net.therap.springmybatis.controller;

import net.therap.springmybatis.entity.Person;
import net.therap.springmybatis.service.CompanyService;
import net.therap.springmybatis.service.PersonService;
import net.therap.springmybatis.validator.PersonValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author al-amin
 * @since 1/4/17
 */
@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private PersonValidator personValidator;

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    private static final String HOME = "home";
    private static final String PERSON = "person";
    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String LOGS = "logs";

    private static final String ERROR_MESSAGE = "something went wrong";
    private static final String SUCCESS_MESSAGE_INSERT = "success! person inserted successfully";
    private static final String SUCCESS_MESSAGE_UPDATE = "success! person updated successfully";
    private static final String SUCCESS_MESSAGE_DELETE = "success! person deleted successfully";

    private static final String[] ALLOWED_FIELDS = { "name", "companyId", "joiningDate", "mobileNo", "age"};

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        binder.setValidator(personValidator);
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(false));
        binder.setAllowedFields(ALLOWED_FIELDS);
        //binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(DATE_FORMAT), true));
    }

    @GetMapping("/person/{id}/update")
    public String updatePerson(Model model,
                               @PathVariable(value = "id") int id) {
        model.addAttribute("companyList", companyService.getAllCompany());
        model.addAttribute("person", personService.getPerson(id));

        return PERSON;
    }

    @PostMapping("/person/{id}/update")
    public String updatePerson(HttpSession session,
                               @PathVariable(value = "id") int id,
                               @Valid @ModelAttribute Person person,
                               BindingResult result) {
        person.setId(id);
        if (result.hasErrors()) {
            session.setAttribute("errorLog", ERROR_MESSAGE);
            return REDIRECT_HOME;
        }
        person.setCompany(companyService.getCompany(person.getCompanyId()));

        personService.updatePerson(person);

        log.debug("updated person {}", person.getId());

        session.setAttribute("successLog", SUCCESS_MESSAGE_UPDATE);

        return REDIRECT_HOME;
    }

    @PostMapping("/person/{id}/delete")
    public String deletePerson(HttpSession session,
                               @PathVariable(value = "id") int id) {
        personService.deletePerson(personService.getPerson(id));
        session.setAttribute("successLog", SUCCESS_MESSAGE_DELETE);

        log.debug("deleted person {}", id);

        return REDIRECT_HOME;
    }

    @PostMapping("/person/insert")
    public String insertPerson(HttpSession session,
                               @RequestParam(name = "company_id") int company_id,
                               @ModelAttribute("person") @Valid Person person,
                               BindingResult result) {
        if (result.hasErrors()) {
            session.setAttribute("errorLog", ERROR_MESSAGE);
            return REDIRECT_HOME;
        }

        System.out.println("date: " + person.getJoiningDate());
        System.out.println("mobile: " + person.getMobileNo());
        System.out.println("age: " + person.getAge());

        person.setCompany(companyService.getCompany(company_id));
        personService.insertPerson(person);
        log.debug("inserted a new person id={}, name={}, compamy_id={}", person.getId(), person.getName(), person.getCompany().getId());
        session.setAttribute("successLog", SUCCESS_MESSAGE_INSERT);

        return REDIRECT_HOME;
    }

}
