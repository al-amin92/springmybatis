package net.therap.springmybatis.controller;

import net.therap.springmybatis.entity.Company;
import net.therap.springmybatis.entity.Person;
import net.therap.springmybatis.service.CompanyService;
import net.therap.springmybatis.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author al-amin
 * @since 1/4/17
 */
@Controller
public class HomeController {

    @Autowired
    private PersonService personService;

    @Autowired
    private CompanyService companyService;

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    private static final String HOME = "home";
    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String LOGS = "logs";

    @GetMapping("/")
    public String goHome() {
        return REDIRECT_HOME;
    }

    @GetMapping("/home")
    public String getPersons(Model model) {
        model.addAttribute("personList", personService.getAllPerson());
        model.addAttribute("person", new Person());
        model.addAttribute("companyList", companyService.getAllCompany());
        model.addAttribute("company", new Company());

        return HOME;
    }

    @GetMapping("/logs")
    public String getLogs(Model model) {

        List<String> logs = new ArrayList<>();

        logs.add(log.toString());
        model.addAttribute("logs", logs);

        return LOGS;
    }

}
