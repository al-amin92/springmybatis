package net.therap.springmybatis.controller;

import net.therap.springmybatis.entity.Company;
import net.therap.springmybatis.service.CompanyService;
import net.therap.springmybatis.validator.CompanyValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author al-amin
 * @since 1/4/17
 */
@Controller
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyValidator companyValidator;

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    private static final String COMPANY = "company";
    private static final String REDIRECT_HOME = "redirect:/home";

    private static final String ERROR_MESSAGE = "something went wrong";
    private static final String SUCCESS_MESSAGE_INSERT = "success! company inserted successfully";
    private static final String SUCCESS_MESSAGE_UPDATE = "success! company updated successfully";
    private static final String SUCCESS_MESSAGE_DELETE = "success! company deleted successfully";
    private static final String ERROR_MESSAGE_REFERENCE_EXISTS = "error! one or more person reference(s) the company." +
            "The company can't be deleted.";

    private static final String[] ALLOWED_FIELDS = {"name", "details", "country"};

    @InitBinder
    public void dataBinding(WebDataBinder binder) {
        binder.setValidator(companyValidator);
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(false));
        binder.setAllowedFields(ALLOWED_FIELDS);
    }

    @PostMapping("/company/insert")
    public String insertCompany(HttpSession session,
                                @ModelAttribute("company") @Valid Company company,
                                BindingResult result) {
        if (result.hasErrors()) {
            session.setAttribute("errorLog", ERROR_MESSAGE);
            return REDIRECT_HOME;
        }

        System.out.println("company country: " + company.getCountry());

        companyService.insertCompany(company);
        log.debug("inserted a new company id={}, name={}, details={}", company.getId(), company.getName(), company.getDetails());
        session.setAttribute("successLog", SUCCESS_MESSAGE_INSERT);

        return REDIRECT_HOME;
    }

    @GetMapping("/company/{id}/update")
    public String updateComapany(Model model,
                                 @PathVariable(value = "id") int id) {
        model.addAttribute("company", companyService.getCompany(id));
        return COMPANY;
    }

    @PostMapping("/company/{id}/update")
    public String updateCompany(HttpSession session,
                                @PathVariable(value = "id") int id,
                                @ModelAttribute("company") @Valid Company company,
                                BindingResult result) {
        company.setId(id);
        if (result.hasErrors()) {
            session.setAttribute("errorLog", ERROR_MESSAGE);
            return REDIRECT_HOME;
        }

        companyService.updateCompany(company);

        log.debug("updated company {}", id);

        session.setAttribute("successLog", SUCCESS_MESSAGE_UPDATE);

        return REDIRECT_HOME;
    }

    @PostMapping("/company/{id}/delete")
    public String deleteCompany(HttpSession session,
                                @PathVariable(value = "id") int id) {
        try {
            companyService.deleteCompany(companyService.getCompany(id));
            session.setAttribute("successLog", SUCCESS_MESSAGE_DELETE);

            log.debug("deleted company {}", id);
        } catch (Exception e) {
            session.setAttribute("errorLog", ERROR_MESSAGE_REFERENCE_EXISTS);
        }
        return REDIRECT_HOME;
    }

}
