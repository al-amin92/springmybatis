package net.therap.springmybatis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author al-amin
 * @since 1/12/17
 */
@Controller
public class TableController {

    @GetMapping(value = "/table")
    public String showTable() {
        return "table";
    }

}
