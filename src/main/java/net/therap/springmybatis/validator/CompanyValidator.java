package net.therap.springmybatis.validator;

import net.therap.springmybatis.entity.Company;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author al-amin
 * @since 1/8/17
 */
@Component
public class CompanyValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Company.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Company company = (Company) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "", "Username is empty");
        if (company.getName().length() < 5) {
            errors.rejectValue("name", "", "name length is less than 5");
        }
    }
}
