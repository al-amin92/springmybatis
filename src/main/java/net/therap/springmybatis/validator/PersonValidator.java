package net.therap.springmybatis.validator;

import net.therap.springmybatis.entity.Person;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author al-amin
 * @since 1/8/17
 */
@Component
public class PersonValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Person.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Person person = (Person) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "", "Username is empty");
        if (person.getName().length() < 5) {
            errors.rejectValue("name", "", "name length is less than 5");
        }
    }
}
