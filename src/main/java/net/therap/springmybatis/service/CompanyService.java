package net.therap.springmybatis.service;

import net.therap.springmybatis.dao.CompanyDao;
import net.therap.springmybatis.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author al-amin
 * @since 1/5/17
 */
@Service
public class CompanyService {

    @Autowired
    private CompanyDao companyDao;

    public List<Company> getAllCompany() {
        return companyDao.getAllCompany();
    }

    public Company getCompany(int id) {
        return companyDao.getCompany(id);
    }

    public void insertCompany(Company company) {
        companyDao.insert(company);
    }

    public void deleteCompany(Company company) {
        companyDao.delete(company);
    }

    public void updateCompany(Company company) {
        companyDao.update(company);
    }
}
