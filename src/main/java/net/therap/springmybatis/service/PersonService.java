package net.therap.springmybatis.service;

import net.therap.springmybatis.dao.PersonDao;
import net.therap.springmybatis.entity.Person;
import net.therap.springmybatis.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author al-amin
 * @since 1/3/17
 */
@Service
public class PersonService {

    @Autowired
    private PersonDao personDao;

    @Autowired
    private Mapper mapper;

    public List<Person> selectAllPerson() {
        return mapper.selectAllPerson();
    }

    public List<Person> getAllPerson() {
        return personDao.getAllPerson();
    }

    public Person getPerson(int id) {
        return personDao.getPersonById(id);
    }

    @Transactional
    public void insertPerson(Person person) {
        personDao.insertPerson(person);
    }

    @Transactional
    public void updatePerson(Person person) {
        personDao.updatePerson(person);
    }

    @Transactional
    public void deletePerson(Person person) {
        personDao.deletePerson(person);
    }
}
