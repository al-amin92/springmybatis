package net.therap.springmybatis.service;

import net.therap.springmybatis.entity.Details;
import net.therap.springmybatis.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author al-amin
 * @since 1/3/17
 */
@Service
public class DetailsService {

    @Autowired
    private Mapper mapper;

    public Details selectDetails(int id) {
        return mapper.selectDetails(id);
    }

}
