<%--
  Created by IntelliJ IDEA.
  User: al-amin
  Date: 1/12/17
  Time: 3:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Show/Hide Table Rows</title>
    <style>
        td {
            vertical-align: top;
        }
        #table1, #table2, #table3, #table4 {
            display: none;
        }
    </style>
    <script>
        function show(nr) {
            document.getElementById("table1").style.display="none";
            document.getElementById("table2").style.display="none";
            document.getElementById("table3").style.display="none";
            document.getElementById("table4").style.display="none";
            document.getElementById("table"+nr).style.display="block";
        }
    </script>
</head>
<body>
<div>
    <table>
        <tr>
            <td>
                <a href="#" onclick='show(1);'>Table 1</a>
                <br />
                <a href="#" onclick='show(2);'>Table 2</a>
                <br />
                <a href="#" onclick='show(3);'>Table 3</a>
                <br />
                <a href="#" onclick='show(4);'>Table 4</a>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <div id="table1"> Content of 1 </div>
                <div id="table2"> Content of 2 </div>
                <div id="table3"> Content of 3 </div>
                <div id="table4"> Content of 4 </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
